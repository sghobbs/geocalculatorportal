﻿namespace GoogleApiService.Clients.SerializationStrategies
{
    public class ApiOutputFormat
    {
        private ApiOutputFormat(string formatName)
        {
            Name = formatName;
        }

        public string Name { get; }

        public override string ToString()
        {
            return Name;
        }

        public static ApiOutputFormat Json = new ApiOutputFormat("json");
        public static ApiOutputFormat Xml = new ApiOutputFormat("xml");
    }
}