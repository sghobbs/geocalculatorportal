﻿namespace GoogleApiService.Clients.SerializationStrategies
{
    public interface ISerializationStrategy
    {
        ApiOutputFormat SerializationFormat { get; }
        T DeserializeObject<T>(string objString);
    }
}