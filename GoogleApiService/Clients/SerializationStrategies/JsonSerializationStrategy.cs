﻿using Newtonsoft.Json;

namespace GoogleApiService.Clients.SerializationStrategies
{
    public class JsonSerializationStrategy : ISerializationStrategy
    {
        public ApiOutputFormat SerializationFormat => ApiOutputFormat.Json;
        public T DeserializeObject<T>(string objString)
        {
            return JsonConvert.DeserializeObject<T>(objString);
        }
    }
}