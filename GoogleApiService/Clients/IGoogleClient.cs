﻿using System.Threading.Tasks;
using GoogleApiService.Requests;
using GoogleApiService.Responses;

namespace GoogleApiService.Clients
{
    public interface IGoogleClient
    {
        Task<TResponse> GetApiResponseAsync<TResponse>(IGoogleApiRequest<TResponse> request)
            where TResponse : IGoogleApiResponse;
    }
}
