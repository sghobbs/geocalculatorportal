﻿using System.Net.Http;
using System.Threading.Tasks;
using GoogleApiService.Clients.SerializationStrategies;
using GoogleApiService.Requests;
using GoogleApiService.Responses;

namespace GoogleApiService.Clients
{
    public class GoogleClient : IGoogleClient
    {
        private readonly HttpClient _httpClient;
        private readonly ISerializationStrategy _serializationStrategy;

        public GoogleClient(HttpClient httpClient, ISerializationStrategy serializationStrategy)
        {
            _httpClient = httpClient;
            _serializationStrategy = serializationStrategy;
        }

        public async Task<TResponse> GetApiResponseAsync<TResponse>(IGoogleApiRequest<TResponse> request) where TResponse : IGoogleApiResponse
        {
            var uri = request.GetRequestUri(_serializationStrategy.SerializationFormat);
            var response = await _httpClient.GetAsync(uri);
            var contentString = await response.Content.ReadAsStringAsync();
            return _serializationStrategy.DeserializeObject<TResponse>(contentString);
        }
    }
}