﻿namespace GoogleApiService.Responses
{
    public enum GoogleApiStatus
    {
        OK,
        INVALID_REQUEST,
        MAX_ELEMENTS_EXCEEDED,
        OVER_QUERY_LIMIT,
        REQUEST_DENIED,
        UKNOWN_ERROR,
        NOT_FOUND,
        ZERO_RESULTS
    }
}