﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace GoogleApiService.Responses
{
    public class GoogleDistanceMatrixResponse : GoogleApiResponse
    {
        public string[] Destination_Addresses { get; set; }
        public string[] Origin_Addresses { get; set; }
        public Row[] Rows { get; set; }
    }

    public class Row
    {
        public Element[] Elements { get; set; }
    }

    public class Element
    {
        public Distance Distance { get; set; }
        public Duration Duration { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public GoogleApiStatus Status { get; set; }

    }

    public class Duration
    {
        public string Text { get; set; }
        public int Value { get; set; }
    }

    public class Distance
    {
        public string Text { get; set; }
        public int Value { get; set; }
    }
}