using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace GoogleApiService.Responses
{
    public abstract class GoogleApiResponse : IGoogleApiResponse
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public GoogleApiStatus Status;
    }
}