﻿using System;
using GoogleApiService.Clients.SerializationStrategies;
using GoogleApiService.Responses;

namespace GoogleApiService.Requests
{
    public interface IGoogleApiRequest<TResponse> where TResponse : IGoogleApiResponse
    {
        Uri GetRequestUri(ApiOutputFormat serializationFormat);
    }

    public abstract class GoogleApiRequest<TResponse> : IGoogleApiRequest<TResponse> where TResponse : IGoogleApiResponse
    {
        protected readonly string ApiKey;

        protected GoogleApiRequest(string apiKey)
        {
            ApiKey = apiKey;
        }  

        public Uri GetRequestUri(ApiOutputFormat serializationFormat)
        {
            return new Uri($"https://maps.googleapis.com/maps/{ApiEndpointUrlFragment}/{serializationFormat.Name}?key={ApiKey}&{GetRequestParamsString()}");
        }

        protected abstract string GetRequestParamsString();

        protected abstract string ApiEndpointUrlFragment { get; }
    }
}
