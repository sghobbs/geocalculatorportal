﻿using System.Collections.Generic;
using GoogleApiService.Responses;

namespace GoogleApiService.Requests
{
    public class GoogleDistanceMatrixRequest : GoogleApiRequest<GoogleDistanceMatrixResponse>
    {
        public GoogleDistanceMatrixRequest(string origin, string destination, string apiKey)
            : this(new []{origin}, new []{destination}, apiKey)
        {
        }

        public GoogleDistanceMatrixRequest(IEnumerable<string> origins, IEnumerable<string> destinations, string apiKey) 
            : base(apiKey)
        {
            Origins = JoinLocationStrings(origins);
            Destinations = JoinLocationStrings(destinations);
        }

        public string Origins { get; }
        public string Destinations { get; }

        private static string JoinLocationStrings(IEnumerable<string> locations)
        {
            return string.Join("|", locations);
        }

        protected override string GetRequestParamsString()
        {
            return $"origins={Origins}&destinations={Destinations}";
        }

        protected override string ApiEndpointUrlFragment => "api/distancematrix";
    }
}