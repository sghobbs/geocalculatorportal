﻿using Ninject;

namespace GeoCalculatorPortal
{
    public static class App
    {
        public static IKernel Container { get; } = new StandardKernel();
    }
}