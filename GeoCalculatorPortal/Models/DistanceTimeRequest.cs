﻿namespace GeoCalculatorPortal.Models
{
    public class DistanceTimeRequest
    {
        public string StartAddress { get; set; }
        public string EndAddress { get; set; }
    }
}