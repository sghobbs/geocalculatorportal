﻿namespace GeoCalculatorPortal.Models
{
    public class DistanceTimeResult
    {
        public string StartAddress { get; set; }
        public string EndAddress { get; set; }
        public string Distance { get; set; }
        public string TravelTime { get; set; }
    }
}