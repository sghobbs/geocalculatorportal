var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var GeoCalculatorViewModel = (function (_super) {
    __extends(GeoCalculatorViewModel, _super);
    function GeoCalculatorViewModel() {
        var _this = this;
        _super.call(this);
        this.startAddress = "";
        this.endAddress = "";
        this.requestResults = [];
        this.onSubmit = function () {
            if (_this.isValid()) {
                $.get("/api/GeoCalculator", _this.toRequest())
                    .done(_this.onRequestSuccess)
                    .fail(_this.onRequestFail);
            }
            else {
                _this.setErrorMessage("Start location and end location cannot be empty.");
            }
        };
        this.onRequestSuccess = function (response) {
            _this.set("errorMessage", null);
            _this.get("requestResults").push(response);
        };
        this.onRequestFail = function (response) {
            _this.setErrorMessage("An error has occurred while calculating your request. Please try again.");
        };
        this.toRequest = function () {
            return {
                startAddress: _this.startAddress,
                endAddress: _this.endAddress
            };
        };
        this.isValid = function () {
            return _this.checkStringIsNotEmpty(_this.startAddress)
                && _this.checkStringIsNotEmpty(_this.endAddress);
        };
        this.checkStringIsNotEmpty = function (str) {
            return str !== undefined &&
                str !== null &&
                str.trim() !== "";
        };
        this.errorMessage = null;
        this.hasError = function () {
            return _this.checkStringIsNotEmpty(_this.get("errorMessage"));
        };
        this.setErrorMessage = function (message) {
            _this.set("errorMessage", message);
        };
        _super.prototype.init.call(this, this);
    }
    return GeoCalculatorViewModel;
}(kendo.data.ObservableObject));
$(function () {
    var vm = new GeoCalculatorViewModel();
    kendo.bind("#calculator-section", vm);
});
//# sourceMappingURL=GeoCalculatorViewModel.js.map