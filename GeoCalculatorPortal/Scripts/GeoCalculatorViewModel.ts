﻿interface IDistanceTimeResult {
    Distance: string;
    TravelTime: string;
    StartAddress: string;
    EndAddress: string;
}

class GeoCalculatorViewModel extends kendo.data.ObservableObject {
    startAddress: string = "";
    endAddress: string = "";

    requestResults: IDistanceTimeResult[] = [];

    constructor() {
        super();
        super.init(this);
    }

    onSubmit = () => {
        if (this.isValid()) {
            $.get("/api/GeoCalculator", this.toRequest())
                .done(this.onRequestSuccess)
                .fail(this.onRequestFail);
        } else {
            this.setErrorMessage("Start location and end location cannot be empty.");
        }      
    }

    onRequestSuccess = (response: IDistanceTimeResult) => {
        this.set("errorMessage", null);
        this.get("requestResults").push(response);
    }

    onRequestFail = (response: any) => {
        this.setErrorMessage("An error has occurred while calculating your request. Please try again.");
    }

    toRequest = () => {
        return {
            startAddress: this.startAddress,
            endAddress: this.endAddress
        }
    }

    isValid = () => {
        return this.checkStringIsNotEmpty(this.startAddress)
            && this.checkStringIsNotEmpty(this.endAddress);
    }

    checkStringIsNotEmpty = (str: string) => {
        return str !== undefined &&
            str !== null &&
            str.trim() !== "";
    }

    errorMessage: string = null;

    hasError = () => {
        return this.checkStringIsNotEmpty(this.get("errorMessage"));
    }

    setErrorMessage = (message: string) => {
        this.set("errorMessage", message);
    }
}

$(() => {
    var vm = new GeoCalculatorViewModel();
    kendo.bind("#calculator-section", vm);
})