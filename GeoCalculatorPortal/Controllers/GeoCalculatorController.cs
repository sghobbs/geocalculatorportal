﻿using System.Threading.Tasks;
using GeoCalculatorPortal.Models;
using System.Web.Http;
using GeoCalculatorPortal.Services;
using Ninject;

namespace GeoCalculatorPortal.Controllers
{
    public class GeoCalculatorController : ApiController
    {
        private readonly IGeoCalculationService _calculationService;

        //A default controller with no parameters is necessary for Web API's controller factory to function
        public GeoCalculatorController():this(null)
        {
            
        }

        public GeoCalculatorController(IGeoCalculationService geoCalculationService)
        {
            _calculationService = geoCalculationService ?? App.Container.Get<IGeoCalculationService>();
        }

        [HttpGet]
        public async Task<DistanceTimeResult> Get([FromUri]DistanceTimeRequest request)
        {
            return await _calculationService.CalculateDistanceTimeAsync(request);
        }
    }
}
