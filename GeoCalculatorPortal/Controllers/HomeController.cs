﻿using System.Web.Mvc;

namespace GeoCalculatorPortal.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Calculate the distance between two locations!";

            return View();
        }
    }
}
