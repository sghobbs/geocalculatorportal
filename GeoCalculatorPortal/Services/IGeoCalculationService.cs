﻿using System.Threading.Tasks;
using GeoCalculatorPortal.Models;

namespace GeoCalculatorPortal.Services
{
    public interface IGeoCalculationService
    {
        Task<DistanceTimeResult> CalculateDistanceTimeAsync(DistanceTimeRequest request);
    }
}
