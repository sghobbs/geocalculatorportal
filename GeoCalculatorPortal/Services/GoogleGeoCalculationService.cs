﻿using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using GeoCalculatorPortal.Models;
using GoogleApiService.Clients;
using GoogleApiService.Requests;
using GoogleApiService.Responses;

namespace GeoCalculatorPortal.Services
{
    public class GoogleGeoCalculationService : IGeoCalculationService
    {
        private readonly IGoogleClient _googleClient;

        public GoogleGeoCalculationService(IGoogleClient googleClient)
        {
            _googleClient = googleClient;
        }

        public async Task<DistanceTimeResult> CalculateDistanceTimeAsync(DistanceTimeRequest request)
        {
            var apiKey = ConfigurationManager.AppSettings["GoogleApiKey"];
            var distanceMatrixRequest = new GoogleDistanceMatrixRequest(request.StartAddress, request.EndAddress, apiKey);

            var googleResponse = await _googleClient.GetApiResponseAsync(distanceMatrixRequest);

            return GetDistancTimeResultFromGoogleResponse(googleResponse);
        }

        private DistanceTimeResult GetDistancTimeResultFromGoogleResponse(GoogleDistanceMatrixResponse googleResponse)
        {
            var firstElement = googleResponse.Rows.FirstOrDefault()?.Elements.FirstOrDefault();

            return new DistanceTimeResult
            {
                StartAddress = googleResponse.Origin_Addresses.FirstOrDefault(),
                EndAddress = googleResponse.Destination_Addresses.FirstOrDefault(),
                Distance = firstElement?.Distance.Text,
                TravelTime = firstElement?.Duration.Text
            };
        }
    }
}