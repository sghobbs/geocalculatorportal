﻿using System.Net.Http;
using GeoCalculatorPortal.Services;
using GoogleApiService.Clients;
using GoogleApiService.Clients.SerializationStrategies;
using Ninject;

namespace GeoCalculatorPortal
{
    public static class NinjectConfig
    {
        public static void CongifIocKernal(IKernel kernel)
        {
            kernel.Bind<IGeoCalculationService>().To<GoogleGeoCalculationService>().InSingletonScope();
            kernel.Bind<IGoogleClient>().To<GoogleClient>();
            kernel.Bind<HttpClient>().To<HttpClient>();
            kernel.Bind<ISerializationStrategy>().To<JsonSerializationStrategy>().InSingletonScope();
        }
    }
}