﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GoogleApiService.Clients;
using GoogleApiService.Clients.SerializationStrategies;
using GoogleApiService.Requests;
using GoogleApiService.Responses;
using GoogleApiService.Tests.MockJsonResponse;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace GoogleApiService.Tests
{
    [TestClass]
    public class GoogleMapsMatrixTests
    {
        //TODO: Refactor unit test to be more precise about what its testing
        [TestMethod]
        public async Task TestMatrixResponseProducesValidResult()
        {
            GoogleDistanceMatrixResponse expectedResponse = new GoogleDistanceMatrixResponse
            {
                Status = GoogleApiStatus.OK,
                Rows = new [] { new Row { Elements = new []
                {
                    new Element 
                    {
                        Status = GoogleApiStatus.OK,
                        Distance = new Distance { Text = "dist", Value = 111 },
                        Duration = new Duration { Text = "dur", Value = 222 }
                    }
                }} },
                Destination_Addresses = new []{ "dest" },
                Origin_Addresses = new[] { "orig" }
            };
            var response = await GetMockResponse(GetMockHttpJsonResponseFromObject(expectedResponse));

            Assert.AreEqual(expectedResponse.Status, response.Status);
            Assert.IsNotNull(response.Rows);
        }

        //TODO: Refactor unit test to be more precise about what its testing
        [TestMethod]
        public async Task TestResponseObjectMatchesJsonSchema()
        {
            var mockJson = MockJsonData.MockDistanceMatrixJson;
            var response = await GetMockResponse(GetMockHttpJsonResponseFromObject(mockJson));

            Assert.IsNotNull(response?.Rows);
        }

        private async Task<GoogleDistanceMatrixResponse> GetMockResponse(HttpResponseMessage mockMessageJsonResponse)
        {
            var jsonStrategy = new JsonSerializationStrategy();
            var request = GetMockGoogleMapsMatrixRequest();
            var fakeHttpHandler = new FakeResponseHandler();
            fakeHttpHandler.AddFakeResponse(request.GetRequestUri(jsonStrategy.SerializationFormat), mockMessageJsonResponse);
            var client = new GoogleClient(new HttpClient(fakeHttpHandler), jsonStrategy);

            return await client.GetApiResponseAsync(request);
        }

        private GoogleDistanceMatrixRequest GetMockGoogleMapsMatrixRequest()
        {
            return new GoogleDistanceMatrixRequest(new[] { "origin1" }, new[] { "destination1" }, "mockKey");
        }

        private HttpResponseMessage GetMockHttpJsonResponseFromObject(object obj)
        {
            return GetMockHttpJsonResponseFromObject(JsonConvert.SerializeObject(obj));
        }

        private HttpResponseMessage GetMockHttpJsonResponseFromObject(string str)
        {
            HttpContent content = new StringContent(str, Encoding.UTF8, "application/json");
            return new HttpResponseMessage(HttpStatusCode.OK) { Content = content };
        }
    }

    //Source: http://stackoverflow.com/questions/22223223/how-to-pass-in-a-mocked-httpclient-in-a-net-test
    public class FakeResponseHandler : DelegatingHandler
    {
        private readonly Dictionary<Uri, HttpResponseMessage> _FakeResponses = new Dictionary<Uri, HttpResponseMessage>();

        public void AddFakeResponse(Uri uri, HttpResponseMessage responseMessage)
        {
            _FakeResponses.Add(uri, responseMessage);
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            return
                Task.FromResult(_FakeResponses.ContainsKey(request.RequestUri)
                    ? _FakeResponses[request.RequestUri]
                    : new HttpResponseMessage(HttpStatusCode.NotFound) {RequestMessage = request});
        }
    }
}
