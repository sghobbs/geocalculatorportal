# README #

## The Story ##
The **Geo Calculator Portal** is a web-based tool for calculating the distance and travel time between two locations. Calculations can be processed either via a UI or a call to a REST API. The app uses the [Google Maps Distance Matrix API](https://developers.google.com/maps/documentation/distance-matrix/) on the backend for its calculations, but other calculation providers (Bing, etc.) could be easily swapped in with minimal changes to the app code.


## Architecture ##
### GeoCalculatorPortal project ###
An ASP.NET web app. Views are served using MVC, but the calculation requests themselves are handled by a Web API REST endpoint. The Web API controller for this endpoint, [GeoCalculatorController](https://bitbucket.org/sghobbs/geocalculatorportal/src/f4af38481c12bf300e44081b6a8860b41ff5b7ec/GeoCalculatorPortal/Controllers/GeoCalculatorController.cs?at=master&fileviewer=file-view-default), defers calculations to an IGeoCalculationService abstraction.

#### IGeoCalculationService ####
An abstraction of the service used for actually calculating distances and travel times. In this version of the app, [IGeoCalculationService](https://bitbucket.org/sghobbs/geocalculatorportal/src/f4af38481c12bf300e44081b6a8860b41ff5b7ec/GeoCalculatorPortal/Services/IGeoCalculationService.cs?at=master&fileviewer=file-view-default) has one concrete implementation, [GoogleGeoCalculationService](https://bitbucket.org/sghobbs/geocalculatorportal/src/f4af38481c12bf300e44081b6a8860b41ff5b7ec/GeoCalculatorPortal/Services/GoogleGeoCalculationService.cs?at=master&fileviewer=file-view-default), which is an adapter class that encapsulates references to the GoogleApiServices library.

#### IoC Container Configuration ####
Contextual dependency injection for C# classes is handled by the Ninject library. To configure the Ninject kernal, see [/App_Start/NinjectConfig.cs](https://bitbucket.org/sghobbs/geocalculatorportal/src/f4af38481c12bf300e44081b6a8860b41ff5b7ec/GeoCalculatorPortal/App_Start/NinjectConfig.cs?at=master&fileviewer=file-view-default).

#### Client-side Libraries ####
The app UI uses Twitter's Bootstrap library for styling and viewport responsiveness. Telerik's open-source Kendo UI Core JavaScript library is used for client-side MVVM model-binding. JavaScript models were composed using TypeScript, found in [/Scripts/GeoCalculatorViewModel.ts](https://bitbucket.org/sghobbs/geocalculatorportal/src/f4af38481c12bf300e44081b6a8860b41ff5b7ec/GeoCalculatorPortal/Scripts/GeoCalculatorViewModel.ts?at=master&fileviewer=file-view-default).

### GoogleApiService project ###
This library facilitates the construction, submission and parsing of Google API requests and responses.

#### GoogleClient ####
The [GoogleClient](https://bitbucket.org/sghobbs/geocalculatorportal/src/f4af38481c12bf300e44081b6a8860b41ff5b7ec/GoogleApiService/Clients/GoogleClient.cs?at=master&fileviewer=file-view-default) class is composed of the following key members:

+ An [HttpClient](https://msdn.microsoft.com/en-us/library/system.net.http.httpclient(v=vs.118).aspx), which processes the actual HTTP requests.

+ An [ISerializationStrategy](https://bitbucket.org/sghobbs/geocalculatorportal/src/f4af38481c12bf300e44081b6a8860b41ff5b7ec/GoogleApiService/Clients/SerializationStrategies/ISerializationStrategy.cs?at=master&fileviewer=file-view-default), which assists the client in constructing its request URIs and parsing API responses.

#### IGoogleApiRequest/IGoogleApiResponse ####
The [IGoogleApiRequest](https://bitbucket.org/sghobbs/geocalculatorportal/src/f4af38481c12bf300e44081b6a8860b41ff5b7ec/GoogleApiService/Requests/IGoogleApiRequest.cs?at=master&fileviewer=file-view-default) interface represents an API request and its parameters. It has a generic type parameter of TResponse, which is the type of [IGoogleApiResponse](https://bitbucket.org/sghobbs/geocalculatorportal/src/f4af38481c12bf300e44081b6a8860b41ff5b7ec/GoogleApiService/Responses/IGoogleApiResponse.cs?at=master&fileviewer=file-view-default) that this request expects.

Currently, the only request type implemented is that for the [Google Maps Distance Matrix API](https://developers.google.com/maps/documentation/distance-matrix/intro), but new implementations can be easily added by extending the [GoogleApiRequest](https://bitbucket.org/sghobbs/geocalculatorportal/src/f4af38481c12bf300e44081b6a8860b41ff5b7ec/GoogleApiService/Requests/IGoogleApiRequest.cs?at=master&fileviewer=file-view-default) and [GoogleApiResponse](https://bitbucket.org/sghobbs/geocalculatorportal/src/f4af38481c12bf300e44081b6a8860b41ff5b7ec/GoogleApiService/Responses/GoogleApiResponse.cs?at=master&fileviewer=file-view-default) abstract classes.


## Setting Up ##
* Open up GeoCalculatorPortal.sln in Visual Studio and build the solution.
	+ **Note:** Visual Studio should automatically handle downloading NuGet dependencies when building for the first time. If not, you may need to manually restore NuGet packages for the solution.
* Set the GoogleApiKey app setting property in [/GeoCalculatorPortal/web.config](https://bitbucket.org/sghobbs/geocalculatorportal/src/f4af38481c12bf300e44081b6a8860b41ff5b7ec/GeoCalculatorPortal/Web.config?at=master&fileviewer=file-view-default) to your own Google API key.
    + **Note:** For this app, make sure that the [Google Distance Matrix API](https://developers.google.com/maps/documentation/distance-matrix/) service is enabled for the API key provided.
* You're set! Debug, deploy, destroy!


## Tests ##
MSBuild-compatible unit tests for the GoogleApiService project are contained within the GoogleApiService.Tests project. The tests are not comprehensive nor particularly granular, but they do give a good baseline check to ensure that functions important to the GeoCalculatorPortal behave as expected.


## Opportunities to Extend ##
* To replace Google Maps with a different provider, implement IGeoCalculationService.
* To add new API operations to the GoogleApiService, extend the GoogleApiRequest and GoogleApiResponse abstract classes.